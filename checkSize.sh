#!/usr/bin/env bash

if [ "$#" -ne 1 ]; then
    echo "Please entre maximum size as argument (ex : 2M)"
    exit 1
fi

maxSize=$1
biggerFiles=$(find . -type f -size +${maxSize})

if [[ -n ${biggerFiles} ]]; then
    echo "Your files are bigger than ${maxSize} :"
    echo ${biggerFiles}
    exit 1
fi
#!/usr/bin/env bash

#get the number of the current day in the week
day=`date +%u`

if [[ $day -eq 5 ]]; then
    echo "Sorry, no merge on Friday"
    exit 1
fi
